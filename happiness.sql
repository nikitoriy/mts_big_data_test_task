SELECT max(val) as val, country_code as entity_name, "country" as entity_type, "max" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Countries.country_code
		FROM Tweet
		JOIN Locations ON Tweet.location_id = Locations.location_id
        JOIN Countries ON Locations.country_id = Countries.country_id
		GROUP BY country_code)
UNION
SELECT min(val) as val, country_code as entity_name, "country" as entity_type, "min" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Countries.country_code
		FROM Tweet
		JOIN Locations ON Tweet.location_id = Locations.location_id
        JOIN Countries ON Locations.country_id = Countries.country_id
		GROUP BY country_code)
UNION
SELECT max(val) as val, location_name as entity_name, "location" as entity_type, "max" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Locations.location_name
		FROM Tweet
		JOIN Locations ON Tweet.location_id = Locations.location_id
		GROUP BY location_name)
UNION
SELECT min(val) as val, location_name as entity_name, "location" as entity_type, "min" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Locations.location_name
		FROM Tweet
		JOIN Locations ON Tweet.location_id = Locations.location_id
		GROUP BY location_name)
UNION
SELECT max(val) as val, name as entity_name, "user" as entity_type, "max" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Users.name
		FROM Tweet
		JOIN Users ON Tweet.user_id = Users.user_id
		GROUP BY name)
UNION
SELECT min(val) as val, name as entity_name, "user" as entity_type, "min" as min_or_max
	FROM (SELECT sum(tweet_sentiment) as val, Users.name
		FROM Tweet
		JOIN Users ON Tweet.user_id = Users.user_id
		GROUP BY name);