# -*- coding: utf-8 -*-

import json
import sqlite3


def extract_data(string):
    data = json.loads(string)
    if 'delete' in data.keys():
        return {'delete': True}
    else:
        return {
            'name': data['user']['screen_name'],
            'tweet_text': data['text'],
            'lang': data['lang'],
            'created_at': data['created_at'],
            'display_url': f'https://twitter.com/{data["user"]["screen_name"]}/status/{data["id"]}',
            'country_code': data['place']['country_code'] if data['place'] is not None and data['place']['country_code'] is not None else None,
            'location': data['place']['name'] if data['place'] is not None and data['place']['name'] is not None else None
        }


def json_reader(jsonfile):
    with open(jsonfile) as tweets_file:
        tweets_data = map(extract_data, tweets_file.readlines())
    return tweets_data


def main():
    db_file = './database.sql'
    create_table_query = """
    CREATE TABLE IF NOT EXISTS Tweet
    (name text,
    tweet_text text,
    country_code text,
    display_url text,
    lang text,
    created_at text,
    location text)
    """
    add_row_query = "INSERT INTO Tweet VALUES(?,?,?,?,?,?,?)"
    add_column_query = 'ALTER TABLE Tweet ADD COLUMN tweet_sentiment INTEGER'

    tweets_data = list(
        filter(
            lambda val: 'delete' not in val.keys(),
            json_reader('./three_minutes_tweets.json.txt')
        )
    )

    con = sqlite3.connect(db_file)
    cur = con.cursor()
    cur.execute(create_table_query)
    cur.executemany(
        add_row_query,
        map(
            lambda data: (
                data['name'],
                data['tweet_text'],
                data['country_code'],
                data['display_url'],
                data['lang'],
                data['created_at'],
                data['location']
            ), tweets_data
        ),
    )
    cur.execute(add_column_query)
    con.commit()
    con.close()


if __name__ == "__main__":
    main()
