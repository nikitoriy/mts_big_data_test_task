ALTER TABLE Tweet RENAME TO Tweet_old;

CREATE TABLE Countries(
  country_id INTEGER PRIMARY KEY AUTOINCREMENT,
  country_code TEXT
);

INSERT INTO Countries(country_code)
SELECT DISTINCT Tweet_old.country_code
FROM Tweet_old
WHERE Tweet_old.country_code NOT IN (SELECT DISTINCT Countries.country_code FROM Countries);


CREATE TABLE Users(
  user_id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT
);

INSERT INTO Users(name)
SELECT DISTINCT Tweet_old.name
FROM Tweet_old
WHERE Tweet_old.name NOT IN (SELECT DISTINCT Users.name FROM Users);

CREATE TABLE Languages(
  language_id INTEGER PRIMARY KEY AUTOINCREMENT,
  language TEXT
);

INSERT INTO Languages(language)
SELECT DISTINCT lang
FROM Tweet_old
WHERE lang NOT IN (SELECT DISTINCT language FROM Languages);

CREATE TABLE Locations(
  location_id INTEGER PRIMARY KEY AUTOINCREMENT,
  location_name TEXT,
  country_id INTEGER,
  FOREIGN KEY(country_id) REFERENCES Countries(country_id)
);

INSERT INTO Locations(location_name, country_id)
SELECT DISTINCT Tweet_old.location, Countries.country_id
FROM Tweet_old
INNER JOIN Countries on Tweet_old.country_code = Countries.country_code
WHERE Tweet_old.location NOT IN (
  SELECT DISTINCT Locations.location_name
  FROM Locations
  INNER JOIN Countries ON Countries.country_id = Locations.country_id
);


CREATE TABLE Tweet(
  tweet_id INTEGER PRIMARY KEY AUTOINCREMENT,
  tweet_text TEXT,
  created_at TEXT,
  display_url TEXT,
  tweet_sentiment INTEGER,
  user_id INTEGER,
  language_id INTEGER,
  location_id INTEGER,
  FOREIGN KEY(user_id) REFERENCES Users(user_id)
  FOREIGN KEY(language_id) REFERENCES Languages(language_id)
  FOREIGN KEY(location_id) REFERENCES Locations(location_id)
);

INSERT INTO Tweet(
  tweet_text,
  created_at,
  display_url,
  tweet_sentiment,
  user_id,
  language_id,
  location_id
)
SELECT
  Tweet_old.tweet_text,
  Tweet_old.created_at,
  Tweet_old.display_url,
  Tweet_old.tweet_sentiment,
  Users.user_id,
  Languages.language_id,
  Locations.location_id
FROM Tweet_old
LEFT JOIN Users ON Users.name = Tweet_old.name
LEFT JOIN Languages ON Languages.language = Tweet_old.lang
LEFT JOIN Locations ON Locations.location_name = Tweet_old.location;

DROP TABLE Tweet_old;
