# -*- coding: utf-8 -*-

import sqlite3
import csv


def get_sentiment(sentiment_file):
    data_single_word = {}
    data_multi_word = {}
    with open(sentiment_file) as data_file:
        data_reader = csv.reader(data_file, delimiter='\t')
        for row in data_reader:
            if ' ' in row[0]:
                data_multi_word[row[0]] = int(row[1])
            else:
                data_single_word[row[0]] = int(row[1])
    return (data_single_word, data_multi_word)


def get_tweets(db_file, query):
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    data = cur.execute(query)
    tweets = data.fetchall()
    con.close()
    return tweets


def calculate_sentiment(str, sentiment_data):
    total_sentiment = 0
    for key in sentiment_data[1]:
        if key in str:
            total_sentiment += sentiment_data[1][key]
            str = str.replace(key, '')
    left_words = str.split()
    for word in left_words:
        if word in sentiment_data[0].keys():
            total_sentiment += sentiment_data[0][word]
    return total_sentiment


def patch_sentiment_in_database(db_file, query, calculated_sentiment):
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    cur.executemany(query, calculated_sentiment)
    con.commit()
    con.close()


def main():
    db_file = './database.sql'
    select_texts_query = 'SELECT tweet_id, tweet_text FROM Tweet'
    update_sentiment_query = 'UPDATE Tweet SET tweet_sentiment= ? WHERE tweet_id= ?'
    sentiment_file = './AFINN-111.txt'

    tweets = get_tweets(db_file, select_texts_query)
    sentiment_data = get_sentiment(sentiment_file)
    calculated_sentiment = map(
        lambda tweet: (
            calculate_sentiment(tweet[1], sentiment_data),
            tweet[0]
        ), tweets
    )
    patch_sentiment_in_database(db_file,
                                update_sentiment_query,
                                calculated_sentiment)


if __name__ == '__main__':
    main()
